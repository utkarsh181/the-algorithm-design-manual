from graph import Graph


def min_weight(graph, start):
    weight = 0
    cur = start
    tree = set()
    distance = {start: 0}
    parent = {}

    while cur not in tree:
        tree.add(cur)

        if cur != start:
            print(f"{parent[cur]} -> {cur} ({dist})")
            weight += dist

        for edge in graph.adj_edges(cur):
            end = edge.end
            if end in tree:
                continue
            elif end not in distance or distance[end] > edge.weight:
                parent[end] = cur
                distance[end] = edge.weight

        next_possible_verts = [(d, v) for v, d in distance.items() if v not in tree]
        if next_possible_verts != []:
            dist, cur = min(next_possible_verts)

    return weight


def max_weight(graph, start):
    weight = 0
    cur = start
    tree = set()
    distance = {start: 0}
    parent = {}

    while cur not in tree:
        tree.add(cur)

        if cur != start:
            print(f"{parent[cur]} -> {cur} ({dist})")
            weight += dist

        for edge in graph.adj_edges(cur):
            end = edge.end
            if end in tree:
                continue
            elif end not in distance or distance[end] < edge.weight:
                parent[end] = cur
                distance[end] = edge.weight

        next_possible_verts = [(d, v) for v, d in distance.items() if v not in tree]
        if next_possible_verts != []:
            dist, cur = max(next_possible_verts)

    return weight


def main():
    # Pg. 248
    graph = Graph(
        (
            (1, 2, 5),
            (1, 3, 7),
            (1, 4, 12),
            (2, 3, 9),
            (2, 5, 7),
            (3, 4, 4),
            (3, 5, 4),
            (3, 6, 3),
            (4, 6, 7),
            (5, 6, 2),
            (5, 7, 5),
            (6, 7, 2),
        ),
    )

    print(min_weight(graph, 1))
    print(max_weight(graph, 1))


if __name__ == "__main__":
    main()

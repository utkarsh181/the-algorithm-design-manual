# TLE (69/71 test cases passed)
# class Solution:
#     def dfs(
#         self,
#         graph: Dict[int, List[int]],
#         discovered: Dict[int, bool],
#         height: Dict[int, List[int]],
#         root: int,
#     ) -> int:
#         maxHeight = 0
#         discovered[root] = True
#         for adjNode in graph.get(root, []):
#             if not discovered.get(adjNode, False):
#                 if not height[root].get(adjNode, False):
#                     height[root][adjNode] = 1 + self.dfs(
#                         graph, discovered, height, adjNode
#                     )
#                 maxHeight = max(maxHeight, height[root][adjNode])
#         return maxHeight

#     def findMinHeightTrees(self, n: int, edges: List[List[int]]) -> List[int]:
#         graph = {}
#         for start, end in edges:
#             graph.setdefault(start, []).append(end)
#             graph.setdefault(end, []).append(start)

#         height = {node: {} for node in graph.keys()}
#         rslt = {}
#         minHeight = float("inf")
#         for root in range(n):
#             curHeight = self.dfs(graph, {}, height, root)
#             if curHeight <= minHeight:
#                 minHeight = curHeight
#                 rslt.setdefault(minHeight, []).append(root)
#         return rslt[minHeight]


class Solution:
    def findMinHeightTrees(self, n: int, edges: List[List[int]]) -> int:
        if n == 1:
            return [0]

        adj = [set() for _ in range(n)]
        for i, j in edges:
            adj[i].add(j)
            adj[j].add(i)

        leaves = [i for i in range(n) if len(adj[i]) == 1]

        while n > 2:
            n -= len(leaves)
            newLeaves = []
            for i in leaves:
                j = adj[i].pop()
                adj[j].remove(i)
                if len(adj[j]) == 1:
                    newLeaves.append(j)
            leaves = newLeaves
        return leaves

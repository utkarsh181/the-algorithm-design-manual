from queue import Queue


class _EdgeNode:
    def __init__(self, end, weight=0, next=None):
        self.end = end
        self.weight = weight
        self.next = next

    def __repr__(self):
        return repr((self.end, self.weight))


class Graph:
    def __init__(self, edges=None, directed=False):
        self._directed = directed
        self._edges = {}
        self._degree = {}
        self._nedges = 0
        if edges:
            for edge in edges:
                self.add_edge(*edge)

    def __len__(self):
        return len(g._edges)

    def __repr__(self):
        rslt = []
        for vert in self._edges:
            adj_verts = self._edges[vert]
            tmp = []
            while adj_verts:
                tmp.append(repr(adj_verts))
                adj_verts = adj_verts.next
            rslt.append(repr(vert) + ": [" + ", ".join(tmp) + "]")
        return "{" + ", ".join(rslt) + "}"

    def get_edges(self):
        for cur in self._edges:
            edge = self._edges[cur]
            while edge:
                yield cur, edge.end, edge.weight
                edge = edge.next

    def add_edge(self, start, end, weight=0):
        def rec(start, end, directed):
            e = _EdgeNode(end, weight, self._edges.get(start, None))
            self._edges[start] = e
            self._degree[start] = self._degree.get(start, 0) + 1

            if not directed:
                rec(end, start, True)
            else:
                self._nedges += 1

        rec(start, end, self._directed)

    def __iter__(self):
        for vert in self._edges.keys():
            yield vert

    def adj_vertices(self, vertex):
        adj_edge = self._edges.get(vertex, False)
        while adj_edge:
            yield adj_edge.end
            adj_edge = adj_edge.next

    def adj_edges(self, vertex):
        adj_edge = self._edges.get(vertex, False)
        while adj_edge:
            yield adj_edge
            adj_edge = adj_edge.next

    def bfs(
        self,
        start=None,
        process_vertex_early=None,
        process_edge=None,
        process_vertex_late=None,
    ):
        if not self._edges:
            return

        if start is None:
            start = next(iter(self._edges))

        discovered = {}
        processed = {}
        parent = {}

        queue = Queue()
        queue.put(start)
        discovered[start] = True

        while not queue.empty():
            cur = queue.get()
            if process_vertex_early:
                process_vertex_early(cur, discovered, processed, parent)
            processed[cur] = True

            adj_verts = self._edges.get(cur, False)
            while adj_verts:
                end = adj_verts.end
                if process_edge and (not processed.get(end, False) or self._directed):
                    process_edge(cur, end, discovered, processed, parent)
                if not discovered.get(end, False):
                    queue.put(end)
                    discovered[end] = True
                    parent[end] = cur
                adj_verts = adj_verts.next

            if process_vertex_late:
                process_vertex_late(cur, discovered, processed, parent)

    def dfs(
        self,
        start=None,
        process_vertex_early=None,
        process_edge=None,
        process_vertex_late=None,
    ):
        def rec(cur, time):
            discovered[cur] = True
            time += 1
            entry_time[cur] = time
            if process_vertex_early:
                process_vertex_early(
                    cur, discovered, processed, parent, entry_time, exit_time
                )

            adj_verts = self._edges.get(cur, False)
            while adj_verts:
                end = adj_verts.end
                if not discovered.get(end, False):
                    parent[end] = cur
                    if process_edge:
                        process_edge(
                            cur,
                            end,
                            discovered,
                            processed,
                            parent,
                            entry_time,
                            exit_time,
                        )
                    time = rec(end, time)
                elif (
                    not processed.get(end, False) and parent[cur] != end
                ) or self._directed:
                    if process_edge:
                        process_edge(
                            cur,
                            end,
                            discovered,
                            processed,
                            parent,
                            entry_time,
                            exit_time,
                        )
                adj_verts = adj_verts.next

            if process_vertex_late:
                process_vertex_late(
                    cur, discovered, processed, parent, entry_time, exit_time
                )
            time += 1
            exit_time[cur] = time
            processed[cur] = True
            return time

        if not self._edges:
            return

        if start is None:
            start = next(iter(self._edges))

        discovered = {}
        processed = {}
        parent = {}
        entry_time = {}
        exit_time = {}
        rec(start, 0)

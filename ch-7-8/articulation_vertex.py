from graph import Graph


def edge_classification(start, end, discovered, processed, parent, entry_time):
    if parent.get(end, False) and parent[end] == start:
        return "tree"
    if discovered.get(end, False) and not processed.get(end, False):
        return "back"
    if processed.get(end, False) and entry_time[end] > entry_time[start]:
        return "forward"
    if processed.get(end, False) and entry_time[end] < entry_time[start]:
        return "cross"


def list_articulation_vertex():
    def process_vertex_early(
        vertex, discovered, processed, parent, entry_time, exit_time
    ):
        reachable_ancestor[vertex] = vertex

    def process_edge(start, end, discovered, processed, parent, entry_time, exit_time):
        eclass = edge_classification(
            start, end, discovered, processed, parent, entry_time
        )
        if eclass and eclass == "tree":
            tree_out_degree[start] = tree_out_degree.get(start, 0) + 1
        if (
            eclass
            and eclass == "back"
            and parent[start] != end
            and entry_time[end] < entry_time[reachable_ancestor[start]]
        ):
            reachable_ancestor[start] = end

    def process_vertex_late(
        vertex, discovered, processed, parent, entry_time, exit_time
    ):
        if not parent.get(vertex, False):
            if tree_out_degree.get(vertex, 0) > 1:
                print(f"Root articulation vertex: {vertex}")
            return

        root = parent.get(parent[vertex], False)
        if root:
            if reachable_ancestor[vertex] == parent[vertex]:
                print(f"Parent articulation vertex: {parent[vertex]}")
            if reachable_ancestor[vertex] == vertex:
                print(f"Bridge articulation vertex: {parent[vertex]}")
                if tree_out_degree.get(vertex, 0) > 0:
                    print(f"Bridge articulation vertex: {vertex}")

        time_vertex = entry_time[reachable_ancestor[vertex]]
        time_parent = entry_time[reachable_ancestor[parent[vertex]]]
        if time_vertex < time_parent:
            reachable_ancestor[parent[vertex]] = reachable_ancestor[vertex]

    reachable_ancestor = {}
    tree_out_degree = {}
    g = Graph(
        (
            (1, 7),
            (1, 8),
            (1, 2),
            (2, 5),
            (2, 7),
            (2, 3),
            (3, 5),
            (3, 4),
            (4, 5),
            (5, 6),
        ),
    )
    g.dfs(1, process_vertex_early, process_edge, process_vertex_late)

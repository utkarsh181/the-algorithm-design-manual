from graph import Graph


class UnionFind:
    def __init__(self) -> None:
        self._parent = {}
        self._size = {}

    def find(self, x: any) -> any:
        if x not in self._parent:
            return x
        if self._parent[x] == x:
            return x

        return self.find(self._parent[x])

    def is_same(self, x: any, y: any) -> bool:
        return self.find(x) == self.find(y)

    def union(self, x: any, y: any) -> None:
        root_x = self.find(x)
        root_y = self.find(y)

        if root_x == root_y:
            return
        elif self._size.get(root_x, 1) >= self._size.get(root_y, 1):
            self._size[root_x] = self._size.get(root_x, 1) + self._size.get(root_y, 1)
            self._parent[root_y] = root_x
        else:
            self._size[root_y] = self._size.get(root_x, 1) + self._size.get(root_y, 1)
            self._parent[root_x] = root_y


def min_weight(graph):
    weight = 0
    union_find = UnionFind()
    edges = [edge for edge in graph.get_edges()]

    edges.sort(key=lambda k: k[2])

    for cur, end, edge_weight in edges:
        if not union_find.is_same(cur, end):
            print(f"{cur} -> {end} ({edge_weight})")
            weight += edge_weight
            union_find.union(cur, end)

    return weight


def max_weight(graph):
    weight = 0
    union_find = UnionFind()
    edges = [edge for edge in graph.get_edges()]

    edges.sort(key=lambda k: k[2], reverse=True)

    for cur, end, edge_weight in edges:
        if not union_find.is_same(cur, end):
            print(f"{cur} -> {end} ({edge_weight})")
            weight += edge_weight
            union_find.union(cur, end)

    return weight


def main():
    # Pg. 248
    graph = Graph(
        (
            (1, 2, 5),
            (1, 3, 7),
            (1, 4, 12),
            (2, 3, 9),
            (2, 5, 7),
            (3, 4, 4),
            (3, 5, 4),
            (3, 6, 3),
            (4, 6, 7),
            (5, 6, 2),
            (5, 7, 5),
            (6, 7, 2),
        ),
    )

    print(min_weight(graph))
    print(max_weight(graph))


if __name__ == "__main__":
    main()

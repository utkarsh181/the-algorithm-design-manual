from queue import Queue
from collections import defaultdict
from enum import Enum


class Color(Enum):
    NO_COLOR = 0
    RED = 1
    BLUE = 2

class Solution:
    def __init__(self):
        self.color = defaultdict(lambda: Color.NO_COLOR)

    def _bfs(self, start: int, graph: list[list[int]]) -> bool:
        if self.color[start] != Color.NO_COLOR:
            return True

        queue = Queue()

        queue.put(start)
        self.color[start] = Color.RED

        while not queue.empty():
            vert = queue.get()
            for adjVert in graph[vert]:
                if self.color[adjVert] == self.color[vert]:
                    return False
                elif self.color[adjVert] == Color.NO_COLOR:
                    queue.put(adjVert)
                    if self.color[vert] == Color.RED:
                        self.color[adjVert] = Color.BLUE
                    else:
                        self.color[adjVert] = Color.RED
        
        return True

    def isBipartite(self, graph: list[list[int]]) -> bool:
        for vert in range(len(graph)):
            if self._bfs(vert, graph) == False:
                return False
        
        return True
from graph import Graph


def shortest_path(graph, start, stop):
    cur = start
    known = set()
    distance = {start: 0}
    parent = {}
    path = []

    while cur not in known:
        known.add(cur)

        if cur == stop:
            break

        for edge in graph.adj_edges(cur):
            end = edge.end
            if end in known:
                continue
            elif end not in distance or distance[end] > distance[cur] + edge.weight:
                parent[end] = cur
                distance[end] = distance[cur] + edge.weight

        next_possible_verts = [(d, v) for v, d in distance.items() if v not in known]
        if next_possible_verts != []:
            _, cur = min(next_possible_verts)

    while cur != start:
        path.append(f"{parent[cur]} -> {cur}")
        cur = parent[cur]

    for edge in reversed(path):
        print(edge)

    return distance[stop]


def longest_path(graph, start, stop):
    cur = start
    known = set()
    distance = {start: 0}
    parent = {}
    path = []

    while cur not in known:
        known.add(cur)

        if cur == stop:
            break

        for edge in graph.adj_edges(cur):
            end = edge.end
            if end in known:
                continue
            elif end not in distance or distance[end] < distance[cur] + edge.weight:
                parent[end] = cur
                distance[end] = distance[cur] + edge.weight

        next_possible_verts = [(d, v) for v, d in distance.items() if v not in known]
        if next_possible_verts != []:
            _, cur = max(next_possible_verts)

    while cur != start:
        path.append(f"{parent[cur]} -> {cur}")
        cur = parent[cur]

    for edge in reversed(path):
        print(edge)

    return distance[stop]


def main():
    # Pg. 248
    graph = Graph(
        (
            (1, 2, 5),
            (1, 3, 7),
            (1, 4, 12),
            (2, 3, 9),
            (2, 5, 7),
            (3, 4, 4),
            (3, 5, 4),
            (3, 6, 3),
            (4, 6, 7),
            (5, 6, 2),
            (5, 7, 5),
            (6, 7, 2),
        ),
    )

    print(shortest_path(graph, 1, 7))
    print(longest_path(graph, 1, 7))


if __name__ == "__main__":
    main()

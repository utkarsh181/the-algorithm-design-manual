from graph import Graph


def transpose_graph(g):
    gt = Graph(directed=True)
    for start in g:
        for end in g.adjacent_vertices(start):
            gt.add_edge(end, start)
    return gt


def strong_components(g):
    def add_vertex(vertex, *rest):
        discovered[vertex] = True

    def add_strong(vertex, *rest):
        if vertex not in discovered:
            tmp.append(vertex)

    discovered = {}
    for vert in dag:
        if vert not in discovered:
            g.dfs(start=vert, process_vertex_late=add_vertex)

    gt = transpose_graph(g)
    discoverd_verts = list(discovered.keys())[::-1]
    discovered = {}
    strongs = []
    for vert in discoverd_verts:
        if vert not in discovered:
            tmp = []
            gt.dfs(vert, add_strong, add_vertex)
            strongs.append(tmp)
    return strongs


dag = Graph(
    (
        (0, 1),
        (1, 2),
        (2, 0),
        (2, 3),
        (3, 4),
        (4, 5),
        (4, 7),
        (5, 6),
        (6, 4),
        (6, 7),
    ),
    True,
)

from graph import Graph


def top_sort(dag):
    def add_vertex(vertex, *rest):
        rslt[vertex] = True

    rslt = {}
    for vert in dag:
        if vert not in rslt:
            dag.dfs(start=vert, process_vertex_late=add_vertex)
    return list(rslt.keys())[::-1]


dag = Graph(
    (
        ("A", "B"),
        ("A", "C"),
        ("B", "C"),
        ("B", "D"),
        ("C", "E"),
        ("C", "F"),
        ("E", "D"),
        ("F", "E"),
        ("G", "A"),
        ("G", "F"),
    ),
    True,
)

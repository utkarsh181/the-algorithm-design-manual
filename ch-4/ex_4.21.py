def median(nums):
    """Find median element

    Parameters
    ----------
    nums: list
        List of int to find median element from

    Returns
    -------
    None or int
        Return median element if possible else None
    """
    def partition(nums, low, high):
        pivot = high
        first_high = low
        for i in range(low, high):
            if nums[i] < nums[pivot]:
                nums[i], nums[first_high] = nums[first_high], nums[i]
                first_high += 1
        nums[pivot], nums[first_high] = nums[first_high], nums[pivot]
        return first_high

    def rec(nums, low, high, median_idx):
        p = partition(nums, low, high)
        if p == median_idx:
            return nums[p]
        elif p > median_idx:
            return rec(nums, low, p - 1, median_idx)
        else:
            return rec(nums, p + 1, high, median_idx)

    n = len(nums)
    if n == 0:
        return None
    else:
        return rec(nums.copy(), 0, n - 1, n // 2)

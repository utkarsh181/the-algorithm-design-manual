#+title: Exercise 4.1
#+author: Utkarsh Singh

This job can be done in O(n) time.  Firstly, find the median element
and then using this as pivot, partition the rest of the data
structure.

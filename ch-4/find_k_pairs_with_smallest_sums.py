from itertools import islice
from heapq import merge

class Solution:
    def kSmallestPairs(self, nums1: List[int], nums2: List[int], k: int) -> List[List[int]]:
        if k > len(nums1) * len(nums2):
            k = len(nums1) * len(nums2)
        streams = map(lambda u: ([u, v] for v in nums2), nums1)
        stream = heapq.merge(*streams, key=sum)
        return [s for s in itertools.islice(stream, k)]

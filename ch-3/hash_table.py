from prime import next_prime

class Hash:
    """A simple chaining Hash Table"""
    def __init__(self, size=3):
        self._key_count = 0
        self._size = size
        self._table = [[] for _ in range(size)]

    def _hash_func(self, key):
        rslt = 17
        mult = 13
        for ch in key:
            rslt += mult * ord(ch)
        return rslt % self._size

    def __getitem__(self, key):
        if not isinstance(key, str):
            raise TypeError

        idx = self._hash_func(key)
        chain = self._table[idx]
        if not chain:
            raise KeyError
        for k, v in chain:
            if k == key:
                return v
        raise KeyError

    def _hash_resize(self):
        self._size = next_prime(2 * self._size)
        ntable = [[] for _ in range(self._size)]
        for chain in self._table:
            for key, value in chain:
                idx = self._hash_func(key)
                ntable[idx].append([key, value])
        self._table = ntable

    def __setitem__(self, key, value):
        if not isinstance(key, str):
            raise TypeError

        load = self._key_count / len(self._table)
        if load > 0.8:
            self._hash_resize()
        idx = self._hash_func(key)
        for node in self._table[idx]:
            if node[0] == key:
                node[1] = value
                return
        self._key_count += 1
        self._table[idx].append([key, value])

def cell(row, col, moves):
    """Return a cell in an Tic Tac Toe board

    Parameters
    ----------
    row : int
        The row of the cell in the board
    col : int
        The column of the cell in the board
    moves : list
        List of player moves

    Returns
    -------
    str
        a string representing the cell
    """

    if [row, col] in moves[0]:
        return "O"
    elif [row, col] in moves[1]:
        return "x"
    return " "


def draw_board(size, moves):
    """Draw the Tic Tac Toe board

    Parameters
    ----------
    size : int
        Size of the board
    moves : list
        List of player moves
    """

    board = "\n"
    for i in range(2 * size - 1):
        if i % 2 == 0:
            for j in range(size):
                board += "| " + cell(i // 2, j, moves) + "  "
            board += "|   "
        else:
            board += " --- " * size
        board += "\n"
    print(board)


def check_move(player, row, col, size, status):
    """Check most recent move

    Parameters
    ---------
    player : int
        Identifier associated to player in status
    row : int
        The row of the cell in the board
    col : int
        The column of the cell in the board
    size : int
        Size of the board
    status : list
        Current status of the board

    Returns
    -------
    bool
        True if current move finishes the game else False
    """
    status[player][0][row] += 1
    status[player][1][col] += 1
    if row == col:
        status[player][2] += 1
    if row + col == size:
        status[player][3] += 1

    if (
        max(status[player][0]) == size
        or max(status[player][1]) == size
        or status[player][2] == size
        or status[player][3] == size
    ):
        return True
    return False


def init_status(size):
    """Initialize status for Tic Tac Board

    Parameters
    ----------
    size : int
        Size of the board

    Returns
    -------
    int
        Status of board
    """

    status = [[], []]
    for i in status:
        for _ in range(2):
            i.append([0] * size)
        i.append(0)
        i.append(0)
    return status


def main():
    print(
        "Tic Tac Toe\n",
        "Instruction:",
        "1. Game involves two player, Player 1 and Player 2.",
        "2. Player 1 gets O symbol.",
        "3. Player 2 gets X symbol.",
        "4. Each player should insert their move in form: [row] [col].",
        sep="\n",
        end="\n\n",
    )
    size = int(input("Enter size of the board: "))
    status = init_status(size)
    moves = [[], []]
    cur_player = 1
    rslt = False
    for _ in range(size * size):
        draw_board(size, moves)
        row, col = map(int, input(f"\nEnter move for Player {cur_player}: ").split())
        moves[cur_player - 1].append([row, col])
        if check_move(cur_player - 1, row, col, size, status):
            print(f"\nPlayer {cur_player} won!")
            rslt = True
            break
        cur_player = 2 if cur_player == 1 else 1
    if not rslt:
        print("\nDraw")


if __name__ == "__main__":
    main()

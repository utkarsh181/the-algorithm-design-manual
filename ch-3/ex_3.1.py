from queue import LifoQueue, Empty

def is_balanced(string):
    "Is parentheses in string balanced?"
    s = LifoQueue()
    try:
        for i, ch in enumerate(string):
            if ch == '(':
                s.put(i)
            elif ch == ')':
                s.get(False)
        return True
    except Empty:
        return (i, ch)


if __name__ == '__main__':
    print(is_balanced('((())())()'))
    print(is_balanced(')()('))
    print(is_balanced('())'))
    
    

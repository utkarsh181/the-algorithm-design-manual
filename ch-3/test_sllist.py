from sllist import SNode, SStack, SQueue
import unittest

class TestSStackMethods(unittest.TestCase):
    def test_push(self):
        s = SStack()
        for c in [1, 2, 3, 4]:
            s.push(c)
        self.assertEqual(s, SStack([4, 3, 2, 1]))

    def test_pop(self):
        s = SStack([1, 2, 3, 4])
        s.pop()
        self.assertEqual(s, SStack([2, 3, 4]))

    def test_reverse(self):
        s = SStack([1, 2, 3, 4])
        s.reverse()
        self.assertEqual(s, SStack([4, 3, 2, 1]))


class TestSQueueMethods(unittest.TestCase):
    def test_enqueue(self):
        s = SQueue()
        for c in [1, 2, 3, 4]:
            s.enqueue(c)
        self.assertEqual(s, SQueue([1, 2, 3, 4]))

    def test_pop(self):
        s = SQueue([1, 2, 3, 4])
        s.dequeue()
        self.assertEqual(s, SQueue([2, 3, 4]))

    def test_reverse(self):
        s = SQueue([1, 2, 3, 4])
        s.reverse()
        self.assertEqual(s, SQueue([4, 3, 2, 1]))


if __name__ == "__main__":
    unittest.main()

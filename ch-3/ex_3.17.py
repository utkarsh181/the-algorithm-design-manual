# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def isBalanced(self, root: Optional[TreeNode]) -> bool:
        def postorder(root: Optional[TreeNode]) -> [int, bool]:
            if not root:
                return -1, True

            l = postorder(root.left)
            r = postorder(root.right)
            depth = max(l[0], r[0]) + 1
            if abs(l[0] - r[0]) <= 1 and l[1] and r[1]:
                return depth, True
            else:
                return depth, False

        return postorder(root)[1]

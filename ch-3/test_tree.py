from tree import TNode, Tree
from queue import Queue, LifoQueue
import unittest


def bft(tree):
    node = t.root
    q = Queue()
    q.put(node)
    while not q.empty():
        tmp = q.get()
        print(tmp)
        if tmp.left:
            q.put(tmp.left)
        if tmp.right:
            q.put(tmp.right)


def dft(tree):
    node = t.root
    q = LifoQueue()
    q.put(node)
    while not q.empty():
        tmp = q.get()
        print(tmp)
        if tmp.right:
            q.put(tmp.right)
        if tmp.left:
            q.put(tmp.left)


class TestTreeMethods(unittest.TestCase):
    def test_min(self):
        t = Tree(range(5))
        self.assertEqual(0, t.min().data)

    def test_max(self):
        t = Tree(range(5))
        self.assertEqual(4, t.max().data)

    def test_insert(self):
        t = Tree()
        for i in [2, 1, 0, 4, 3]:
            t.insert(i)
        self.assertEqual(t, Tree(range(5)))


if __name__ == '__main__':
    unittest.main()

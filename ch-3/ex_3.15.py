# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def balanceBST(self, root: TreeNode) -> TreeNode:
        def inorder(root: TreeNode, elem: int) -> list[int]:
            if not root:
                return elem
            elem += inorder(root.left, [])
            elem.append(root.val)
            elem += inorder(root.right, [])
            return elem

        def list_to_tree(data: list[int], dlen: int) -> TreeNode:
            if dlen == 0:
                return None
            i = dlen // 2
            return TreeNode(data[i],
                            list_to_tree(data[:i], i),
                            list_to_tree(data[i + 1 :], dlen - i - 1))

        data = inorder(root, [])
        root = list_to_tree(data, len(data))
        return root

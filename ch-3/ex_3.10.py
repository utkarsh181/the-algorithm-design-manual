class Solution:
    def isAnagram(self, s: str, t: str) -> bool:
        if len(s) != len(t):
            return False

        table = {}
        for ch in s:
            if ch not in table:
                table[ch] = 1
            else:
                table[ch] += 1
        for ch in t:
            if ch not in table:
                return False
            else:
                table[ch] -= 1
                if table[ch] == -1:
                    return False
        return True

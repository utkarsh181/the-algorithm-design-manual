from dllist import DNode, DStack, DQueue
import unittest


class TestDStackMethods(unittest.TestCase):
    def test_push(self):
        s = DStack()
        for c in [1, 2, 3, 4]:
            s.push(c)
        self.assertEqual(s, DStack([4, 3, 2, 1]))

    def test_pop(self):
        s = DStack([1, 2, 3, 4])
        s.pop()
        self.assertEqual(s, DStack([2, 3, 4]))

    def test_reverse(self):
        s = DStack([1, 2, 3, 4])
        s.reverse()
        self.assertEqual(s, DStack([4, 3, 2, 1]))


class TestDQueueMethods(unittest.TestCase):
    def test_enqueue(self):
        s = DQueue()
        for c in [1, 2, 3, 4]:
            s.enqueue(c)
        self.assertEqual(s, DQueue([1, 2, 3, 4]))

    def test_pop(self):
        s = DQueue([1, 2, 3, 4])
        s.dequeue()
        self.assertEqual(s, DQueue([2, 3, 4]))

    def test_reverse(self):
        s = DQueue([1, 2, 3, 4])
        s.reverse()
        self.assertEqual(s, DQueue([4, 3, 2, 1]))


if __name__ == "__main__":
    unittest.main()

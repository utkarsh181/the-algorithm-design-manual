class DNode:
    def __init__(self, data):
        self.data = data
        self._next = None
        self._prev = None

    def __repr__(self):
        return str(self.data)

    def __lt__(self, other):
        return self.data < other.data

    def __le__(self, other):
        return self.data <= other.data

    def __eq__(self, other):
        return self.data == other.data


class _DLinkedList:
    def __init__(self, data=None):
        self._head = None
        self._tail = None
        self._size = 0
        if data is not None:
            node = DNode(data.pop(0))
            self._head = node
            self._tail = node
            self._size = 1
            for i in data:
                node._next = DNode(i)
                pnode = node
                node = node._next
                node._prev = pnode
                self._tail = node
                self._size += 1

    def __repr__(self):
        if self._head is not None:
            return "dllist([%s])" % ', '.join(repr(i) for i in self)
        else:
            return 'dllist()'

    def __iter__(self):
        node = self._head
        while node is not None:
            yield node
            node = node._next

    def __len__(self):
        return self._size

    def search(self, data):
        node = self._head
        while node is not None:
            if i.data is data:
                return i
            node = node._next
        return None

    def __lt__(self, other):
        slen, olen = len(self), len(other)
        if slen > olen:
            return False
        elif slen < olen:
            return True

        if self == other:
            return False

        for i, j in zip(self, other):
            if i > j:
                return False
        return True

    def __le__(self, other):
        slen, olen = len(self), len(other)
        if slen > olen:
            return False
        elif slen < olen:
            return True

        if self == other:
            return True

        for i, j in zip(self, other):
            if i > j:
                return False
        return True

    def __eq__(self, other):
        if len(self) != len(other):
            return False

        for i, j in zip(self, other):
            if i != j:
                return False
        return True

    def reverse(self):
        prev = None
        cur = self._head
        while cur:
            tmp = cur._next
            cur._next = prev
            cur._prev = tmp
            prev = cur
            cur = tmp
        self._head = prev


class DStack(_DLinkedList):
    def push(self, data):
        node = DNode(data)
        node._next = self._head
        self._head = node
        self._head._prev = node
        self._size += 1

    def pop(self):
        if self._head is None:
            raise Exception("Stack is empty")
        self._head = self._head._next
        self._head._prev = None
        if self._head._next is None:
            self._tail = self._head
        self._size -= 1


class DQueue(_DLinkedList):
    def enqueue(self, data):
        node = DNode(data)
        if self._head is None:
            self._head = node
            self._tail = node
        else:
            self._tail._next = node
            node._prev = self._tail
            self._tail = node
        self._size += 1

    def dequeue(self):
        if self._head is None:
            raise Exception("Queue is empty")

        self._head = self._head._next
        self._head._prev = None
        if self._head._next is None:
            self._tail = self._head
        self._size -= 1

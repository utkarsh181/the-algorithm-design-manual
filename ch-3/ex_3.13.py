# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right

# In-order algorithm by Qiwei Liu at:
# <https://leetcode.com/problems/recover-binary-search-tree/discuss/32535/No-Fancy-Algorithm-just-Simple-and-Powerful-In-Order-Traversal>.
class Solution:
    def recoverTree(self, root: Optional[TreeNode]) -> None:
        def swap(first: TreeNode, second: TreeNode) -> None:
            tmp = first.val
            first.val = second.val
            second.val = tmp

        def inorder(
            root: TreeNode, prev: TreeNode, first: TreeNode, second: TreeNode
        ) -> [TreeNode, TreeNode, TreeNode]:
            if not root:
                return prev, first, second

            prev, first, second = inorder(root.left, prev, first, second)

            if not first and (not prev or prev.val >= root.val):
                first = prev
            if first and prev.val >= root.val:
                second = root
            prev = root

            prev, first, second = inorder(root.right, prev, first, second)
            return prev, first, second

        tmp = inorder(root, None, None, None)
        swap(tmp[1], tmp[2])

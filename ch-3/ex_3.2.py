# Give an algorithm that takes a string S consisting of opening and closing
# parentheses, say )()(())()()))())))(, and ﬁnds the length of the longest
# balanced parentheses in S, which is 12 in the example above.


def longest_contagious_run(string):
    count = 0
    tmp = 0
    for ch in string:
        if ch == ')' and tmp != 0:
            tmp -= 1
            count += 2
        if ch == '(':
            tmp += 1
    return count


if __name__ == '__main__':
    print(longest_contagious_run(')()(())()()))())))('))

            

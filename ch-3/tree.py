class TNode:
    def __init__(self, data, left=None, right=None, parent=None):
        self.data = data
        self.left = left
        self.right = right
        self.parent = parent

    def __repr__(self):
        return str(self.data)

    def __lt__(self, other):
        return self.data < other.data

    def __le__(self, other):
        return self.data <= other.data

    def __eq__(self, other):
        return self.data == other.data


class Tree:
    def __init__(self, data=None):
        def tree(data, dlen):
            if dlen == 0:
                return None
            i = dlen // 2
            return TNode(data[i],
                         tree(data[:i], i),
                         tree(data[i + 1 :], dlen - i - 1))

        def initparent(node, parent):
            node.parent = parent
            if node.left:
                initparent(node.left, node)
            if node.right:
                initparent(node.right, node)

        self.root = None
        self.size = 0
        if data is not None:
            self.size = len(data)
            self.root = tree(data, self.size)
            initparent(self.root, None)

    def __repr__(self, level=0, indent="    "):
        def preorder(node, level):
            s = level * indent + repr(node.data)
            if node.left:
                s = s + "\n" + preorder(node.left, level + 1)
            if node.right:
                s = s + "\n" + preorder(node.right, level + 1)
            return s

        if not self.root:
            return ''
        return preorder(self.root, level)


    def __iter__(self):
        def inorder(node):
            if node:
                for x in inorder(node.left):
                    yield x
                yield node
                for x in inorder(node.right):
                    yield x

        return inorder(self.root)

    def __len__(self):
        return self.size

    def __lt__(self, other):
        slen, olen = len(self), len(other)
        if slen > olen:
            return False
        elif slen < olen:
            return True

        if self == other:
            return False

        for i, j in zip(self, other):
            if i > j:
                return False
        return True

    def __le__(self, other):
        slen, olen = len(self), len(other)
        if slen > olen:
            return False
        elif slen < olen:
            return True

        if self == other:
            return True

        for i, j in zip(self, other):
            if i > j:
                return False
        return True

    def __eq__(self, other):
        if len(self) != len(other):
            return False

        for i, j in zip(self, other):
            if i != j:
                return False
        return True

    def search(self, item):
        def rec(node):
            if not node:
                return None
            elif node.data == item:
                return node
            elif node.data > item:
                return rec(node.left)
            else:
                return rec(node.right)

        return rec(self.root)

    def __contains__(self, item):
        return self.search(item) != None

    def min(self):
        tmp = self.root
        while tmp.left is not None:
            tmp = tmp.left
        return tmp

    def max(self):
        tmp = self.root
        while tmp.right is not None:
            tmp = tmp.right
        return tmp

    def insert(self, item):
        def rec(node, parent, lchild, rchild):
            if node is None:
                node = TNode(item)
                if lchild:
                    node.parent = parent
                    parent.left = node
                elif rchild:
                    node.parent = parent
                    parent.right = node
                else:
                    self.root = node
            elif node.data > item:
                rec(node.left, node, True, False)
            else:
                rec(node.right, node, False, True)

        rec(self.root, None, False, False)
        self.size += 1

    def remove(self, item):
        def predecessor(node):
            if not node.left:
                return None

            pred = node.left
            while pred.right:
                pred = pred.right
            return pred

        def successor(node):
            if not node.right:
                return None

            pred = node.right
            while pred.left:
                pred = pred.left
            return pred

        def rec(node, item):
            rnode = self.search(item)

            if not rnode:
                pass

            if not rnode.parent:
                if not rnode.left and not rnode.right:
                    self.root = None
                    return
                elif rnode.left:
                    pnode = predecessor(rnode)
                else:
                    pnode = successor(rnode)
            elif not rnode.left or not rnode.right:
                if rnode.left:
                    cnode = rnode.left
                else:
                    cnode = rnode.right

                if rnode.parent.left is rnode:
                    rnode.parent.left = cnode
                else:
                    rnode.parent.right = cnode
                return
            else:
                pnode = successor(rnode)

            nitem = pnode.data
            rec(node, pnode.data)
            rnode.data = nitem

        rec(self.root, item)

# Given an integer array nums, reorder it such that
# nums[0] < nums[1] > nums[2] < nums[3]....


class Solution:
    def wiggleSort(self, nums: List[int]) -> None:
        """
        Do not return anything, modify nums in-place instead.
        """
        nums.sort(reverse=True)
        half = len(nums[1::2])
        nums[::2], nums[1::2] = nums[half:], nums[:half]


# Takeaway: Slicing is too cool!

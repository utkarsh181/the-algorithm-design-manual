# Given the head of a linked list, rotate the list to the right by k places.

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next


class Solution:
    def rotateRight(self, head: Optional[ListNode], k: int) -> Optional[ListNode]:
        if head == None:
            return head

        llen = 1
        cur = head
        while cur.next:
            llen += 1
            cur = cur.next
        cur.next = head

        k %= llen
        head_pos = llen - k
        cur = head
        while head_pos > 0:
            prev = cur
            cur = cur.next
            head_pos -= 1
        head = cur
        prev.next = None
        return head


# Takeaway: To rotate a list, you might not actually have to rotate it!

class Solution:
    def countRangeSum(self, nums: list[int], lower: int, upper: int) -> int:
        def sort(lo: int, hi: int) -> int:
            mid = (lo + hi) // 2
            if mid == lo:
                return 0
            count = sort(lo, mid) + sort(mid, hi)
            i = j = mid
            # TODO 2022-11-07: I know how it works, but can I think this?
            for left in first[lo:mid]:
                while i < hi and first[i] - left <  lower: i += 1
                while j < hi and first[j] - left <= upper: j += 1
                count += j - i
            first[lo:hi] = sorted(first[lo:hi])
            return count

        first = [0]
        for num in nums:
            first.append(first[-1] + num)
        return sort(0, len(first))

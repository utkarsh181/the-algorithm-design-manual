class Solution:
    def kth(self,
            nums1: List[int], start1: int, end1: int,
            nums2: List[int], start2: int, end2: int,
            k: int) -> int:
        if start1 > end1:
            return nums2[k - start1]
        if start2 > end2:
            return nums1[k - start2]

        mid1 = (start1 + end1) // 2
        mid2 = (start2 + end2) // 2
        if mid1 + mid2 < k:
            if nums1[mid1] > nums2[mid2]:
                return self.kth(nums1, start1, end1, nums2, mid2 + 1, end2, k)
            else:
                return self.kth(nums1, mid1 + 1, end1, nums2, start2, end2, k)
        else:
            if nums1[mid1] > nums2[mid2]:
                return self.kth(nums1, start1, mid1 - 1, nums2, start2, end2, k)
            else:
                return self.kth(nums1, start1, end1, nums2, start2, mid2 - 1, k)

    def findMedianSortedArrays(self, nums1: List[int], nums2: List[int]) -> float:
        l1 = len(nums1)
        l2 = len(nums2)
        l = l1 + l2
        if l % 2 == 1:
            return self.kth(nums1, 0, l1 - 1, nums2, 0, l2 - 1, l // 2)

        mid1 = self.kth(nums1, 0, l1 - 1, nums2, 0, l2 - 1, l // 2)
        mid2 = self.kth(nums1, 0, l1 - 1, nums2, 0, l2 - 1, l // 2 - 1)
        return (mid1 + mid2) / 2

# Dynamic Programming

class Solution:
    def maxSubArray(self, nums: list[int]) -> int:
        rslt = float('-inf')
        curMax = 0
        for cur in nums:
            curMax = max(cur, cur + curMax)
            rslt = max(rslt, curMax)
        return rslt

# Divide and Conquer

class Solution:
    def maxSubArray(self, nums: list[int]) -> int:
        def rec(low: int, high: int) -> int:
            if low > high:
                return float('-inf')

            mid = (low + high) // 2

            leftSum = cur = 0
            for i in range(mid - 1, low - 1, -1):
                cur += nums[i]
                leftSum = max(leftSum, cur)

            rightSum = cur = 0
            for i in range(mid + 1, high + 1):
                cur += nums[i]
                rightSum = max(rightSum, cur)

            return max(rec(low, mid - 1),
                       rec(mid + 1, high),
                       leftSum + nums[mid] + rightSum)

        return rec(0, len(nums) - 1)

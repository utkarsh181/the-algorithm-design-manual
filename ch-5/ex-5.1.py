def largestAfterKShifts(nums, k):
    return nums[k - 1]

def largestAfterKShifts(nums):
    low = 0
    high = len(nums) - 1

    if nums[low] < nums[high]:
        return nums[high]

    while low <= high:
        mid = (low + high) // 2
        if nums[mid] < nums[high]:
            if nums[mid] < nums[mid - 1]:
                return nums[mid - 1]
            high = mid - 1
        else:
            if nums[mid] > nums[mid + 1]:
                return nums[mid]
            low = mid + 1

def missingNaturalNum(nums):
    low = 0
    high = len(nums) - 1
    while low <= high:
        mid = (low + high) // 2
        if nums[mid] == mid + 1:
            low = mid + 1
        else:
            if nums[mid] - nums[mid - 1] != 1:
                return nums[mid] - 1
            high = mid - 1


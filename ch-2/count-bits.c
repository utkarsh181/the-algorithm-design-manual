/*
 * Given an integer n, return an array ans of length n + 1 such that for each i
 * (0 <= i <= n), ans[i] is the number of 1's in the binary representation of i.
 */

int
ones (int n)
{
  int b;

  for (b = 0; n != 0; n >>= 1)
    if (n & 1)
      b++;
  return b;
}

int *
countBits (int n, int *rsize)
{
  int i, *rslt;

  *rsize = n + 1;
  rslt = (int *) calloc (n + 1, sizeof (int));
  for (i = 0; i <= n; i++)
    rslt[i] = ones (i);
  return rslt;
}

# Given an array nums of n integers, return an array of all the unique
# quadruplets [nums[a], nums[b], nums[c], nums[d]] such that:
#
# - 0 <= a, b, c, d < n
# - a, b, c, and d are distinct.
# - nums[a] + nums[b] + nums[c] + nums[d] == target


class Solution:
    def twoSum(self, nums: list[int], target: int) -> list[list[int]]:
        stack = []
        rslt = []
        for n in nums:
            tmp = target - n
            if tmp in stack:
                rslt.append([tmp, n])
            stack.append(n)
        return rslt

    def fourSum(self, nums: list[int], target: int) -> list[list[int]]:
        nums.sort()
        nlen = len(nums)
        rslt = []
        for i in range(nlen - 3):
            for j in range(i + 1, nlen - 2):
                t1 = [nums[i], nums[j]]
                t2 = self.twoSum(nums[j + 1 : nlen], target - sum(t1))
                for t in t2:
                    t3 = t1 + t
                    if t3 not in rslt:
                        rslt.append(t3)
        return rslt


# Takeaway: Brute force works, but cavet emptor.

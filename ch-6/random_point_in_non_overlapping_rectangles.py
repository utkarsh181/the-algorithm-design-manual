from itertools import accumulate
from bisect import bisect
from random import random, randint

# Thanks to Dmitry Babichev (@DBabichev) for the solution.
class Solution:
    def __init__(self, rects: List[List[int]]):
        self.rects = rects
        sizes = [(xi - ai + 1) * (yi - bi + 1) for ai, bi, xi, yi in rects]
        self.weights = [s / sum(sizes) for s in accumulate(sizes)]

    def pick(self) -> List[int]:
        idx = bisect(self.weights, random())
        rect = self.rects[idx]
        return [randint(rect[0], rect[2]), randint(rect[1], rect[3])]

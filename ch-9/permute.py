class Solution:
    def permute(self, nums: list[int]) -> list[list[int]]:
        if nums == []:
            return [[]]

        rslt = []
        for idx, num in enumerate(nums):
            for p in self.permute(nums[:idx] + nums[idx + 1 :]):
                rslt.append([num] + p)
        return rslt

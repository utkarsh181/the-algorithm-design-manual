from collections import Counter


class Solution:
    BASED = 3

    def _isValidRow(self, row: int, elem: str) -> bool:
        items = Counter(tmp for tmp in self.board[row])
        return items[elem] == 1

    def _isValidCol(self, col: int, elem: str) -> bool:
        items = Counter(tmp[col] for tmp in self.board)
        return items[elem] == 1

    def _isValidSubBox(self, row: int, col: int, elem: str) -> bool:
        rowStart = self.BASED * (row // self.BASED)
        rowEnd = rowStart + self.BASED
        colStart = self.BASED * (col // self.BASED)
        colEnd = colStart + self.BASED
        items = Counter(
            self.board[i][j]
            for j in range(colStart, colEnd)
            for i in range(rowStart, rowEnd)
        )
        return items[elem] == 1

    def isValidSudoku(self, board: list[list[str]]) -> bool:
        self.board = board
        for row, items in enumerate(board):
            for col, elem in enumerate(items):
                if elem == ".":
                    continue
                if not self._isValidRow(row, elem):
                    return False
                if not self._isValidCol(col, elem):
                    return False
                if not self._isValidSubBox(row, col, elem):
                    return False
        return True

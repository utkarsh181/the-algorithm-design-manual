# class Solution:
#     def __init__(self):
#         self.rslt = []

#     def _backtrack(self, subset: list[int], nums: list[int]) -> None:
#         if nums == []:
#             self.rslt.append(subset)
#             return

#         self._backtrack(subset + [nums[0]], nums[1:])
#         self._backtrack(subset, nums[1:])

#     def subsets(self, nums: list[int]) -> list[list[int]]:
#         self._backtrack([], nums)
#         return self.rslt


class Solution:
    def __init__(self):
        self.rslt = []
        self.nums = []

    def _backtrack(self, start: int, subset: list[int]):
        if start > len(self.nums):
            return

        self.rslt.append(subset)
        for idx, num in enumerate(self.nums[start:]):
            self._backtrack(start + idx + 1, subset + [num])

    def subsets(self, nums: list[int]) -> list[list[int]]:
        self.nums = nums
        self._backtrack(0, [])
        return self.rslt
